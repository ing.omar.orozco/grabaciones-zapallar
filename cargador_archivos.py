import os
import os.path, time, datetime
import django
import sys
sys.path.append('/Users/omar/Documents/grabaciones/')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'grabaciones.settings')
django.setup()

from listados.models import *
# import subprocess
from django.utils.timezone import get_current_timezone
# import dateutil.parser
from datetime import datetime as dati


base = '/var/spool/asterisk/monitor'
with os.scandir(base) as ficheros:
     anio = [fichero.name for fichero in ficheros if fichero.is_dir()]
     if anio:
         for a in anio:
             base_anio = base + "/" + a
            #  print(base_anio)
             with os.scandir(base_anio) as ficheros1:
                mes = [fichero.name for fichero in ficheros1 if fichero.is_dir()]
                if mes:
                    for m in mes:
                        base_mes =  base_anio + "/" + m
                        with os.scandir(base_mes) as ficheros2:
                            dia = [fichero.name for fichero in ficheros2 if fichero.is_dir()]
                            if dia:
                                for d in dia:
                                    base_dia = base_mes + "/" + d
                                    with os.scandir(base_dia) as ficheros3:
                                        files = [fichero.name for fichero in ficheros3 if fichero.is_file()]
                                        if files:
                                            for f in files:
                                                ruta_completa = base_dia + "/" + f
                                                nueva = grabaciones()
                                                nueva.name = f
                                                nueva.anio = a
                                                nueva.mes = m
                                                nueva.dia = d
                                                nueva.ruta = ruta_completa

                                                # ti = time.ctime(os.path.getctime(ruta_completa))
                                                modified = os.path.getctime(ruta_completa)
                                                year,month,day,hour,minute,second=time.localtime(modified)[:-3]
                                                fecha = datetime.datetime(year, month, day, hour, minute, second,0,tzinfo=get_current_timezone())
                                                nueva.fecha = fecha
                                                # print(fecha)
                                                # fecha = str(year)+'-'+str(month)+'-'+str(day)+' '+str(hour)+':'+str(minute)+':+58-05'
                                                # nueva.fecha = fecha
                                                # print(fecha)
                                                # print(ti)
                                                # print(hour)
                                                # print(minute)
                                                # tz = get_current_timezone()
                                                # str_date = str(year)+'-'+str(month)+'-'+str(day)+' '+str(hour)+':'+str(minute)+':00'
                                                # dt = tz.localize(dati.strptime(str_date, '%Y-%m-%d %H:%M:%S'))
                                                # print(dt)
                                                nueva.hora = str(hour) + ":"+ str(minute)
                                                try:
                                                    if nueva.save():
                                                        print("registro guardado")
                                                except:
                                                    print("no se puede guardar")
